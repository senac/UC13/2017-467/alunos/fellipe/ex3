
package br.com.senac.exercicio3.test;

import br.com.senac.exercicio3.exercicio3.ConversorDeMoedas;
import org.junit.Test;
import static org.junit.Assert.*;


public class Exercicio3Teste {
    
    public Exercicio3Teste() {
    }
    
    @Test
    public void converterParaDollar(){
        
        ConversorDeMoedas converter = new ConversorDeMoedas();
        
        double real = 100;
        double resultado = converter.conversorDollar(real);
        
        assertEquals(24.44 , resultado, 0.01);
   
    }
    
    @Test
    public void converterParaEuro(){
        
        
        ConversorDeMoedas converter = new ConversorDeMoedas();
        
        double real = 100;
        double resultado = converter.conversorEuro(real);
        
        assertEquals(20.87 , resultado, 0.01);
        
    }
    
    @Test
    public void converterParaDolarAustraliano(){
        
        ConversorDeMoedas converter = new ConversorDeMoedas();
        
        double real = 100;
        double resultado = converter.conversorDolarAustraliano(real);
        
        assertEquals(33.89 , resultado, 0.01);
        
        
    }
    
    
    
}
