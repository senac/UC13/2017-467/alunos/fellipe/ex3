
package br.com.senac.exercicio3.exercicio3;


public class ConversorDeMoedas {
    
    public final double DOLLAR = 4.09;
    public final double EURO = 4.79;
    public final double DOLAR_AUSTRALIANO = 2.95;
    
    
    public double conversorDollar(double real){
        
        double dollar = real / DOLLAR;
        
        
        return dollar;
    }
    
    public  double  conversorEuro(double real){
        double euro = real / EURO;
        
        return euro;
    }
        
        
   public double conversorDolarAustraliano(double real){
       
       double dolarAustraliano = real / DOLAR_AUSTRALIANO;
       
       return dolarAustraliano;
   }
    
    
}
